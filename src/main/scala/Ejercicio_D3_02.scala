import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

object Ejercicio_D3_02 {

  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.builder()
      .appName("Ejercicio_D2_04")
      .master("local[*]")
      .getOrCreate()

    val renfeLogs: RDD[String] = spark.sparkContext.parallelize(List(
      "2020-11-02T10:00:01 1 MADRID BARCELONA AVE 50.00",
      "2020-11-02T10:00:01 2 MADRID MALAGA AVE 40.05",
      "2020-11-02T10:00:01 2 MALAGA MADRID AVE 40.05",
      "2020-11-02T10:00:01 1 BARCELONA MADRID LD 40.08",
    ))
    case class RenfeLog(timestamp: String, customerId: String, origin: String, destination: String, productId: String, price: String)
    val renfeLogsParsed: RDD[RenfeLog] = renfeLogs.map { s: String =>
      val t = s.split(" ")
      RenfeLog(t(0), t(1), t(2), t(3), t(4), t(5))
    }

    // val renfeLogsDF: DataFrame = spark.createDataFrame(renfeLogsParsed)

    val renfeLogsRow = renfeLogsParsed.map(l => Row(l.customerId, l.origin))
    val renfeLogsSchema = StructType(List(
      StructField("customerId", StringType, nullable = true),
      StructField("origin", StringType, nullable = true),
    ))
    val renfeLogsDF2 = spark.createDataFrame(renfeLogsRow, renfeLogsSchema)

    renfeLogsDF2.createOrReplaceTempView("renfeLogs")

    val logsFromCustomer1: DataFrame = spark.sql("SELECT * FROM renfeLogs WHERE customerId == '1'")


  }
}
