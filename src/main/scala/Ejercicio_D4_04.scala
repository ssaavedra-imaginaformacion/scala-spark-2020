import java.io.File

import org.apache.spark.ml.evaluation.{Evaluator, RegressionEvaluator}
import org.apache.spark.ml.recommendation.{ALS, ALSModel}
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

case class Rating(userId: Int,movieId: Int, rating: Double, timestamp: Long)
case class Movie(movieId: Int, title: String, genres: String)

object Ejercicio_D4_04 {

  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.builder()
      .appName("Ejercicio_D2_04")
      .master("local[*]")
      .getOrCreate()

    import spark.implicits._

    val ratings: Dataset[Rating] = spark.read.option("header", true).option("inferSchema", true).csv(filePath("ratings")).as[Rating].cache()
    val movies: Dataset[Movie] = spark.read.option("header", true).option("inferSchema", true).csv(filePath("movies")).as[Movie].cache()

    val Array(train_data: Dataset[Rating], test_data: Dataset[Rating]) = ratings.randomSplit(Array(0.8, 0.2))

    val als = new ALS()
      .setMaxIter(2)
      .setRegParam(0.01)
      .setUserCol("userId")
      .setItemCol("movieId")
      .setRatingCol("rating")

    val model: ALSModel = als.fit(train_data)


    model.setColdStartStrategy("drop")
    val predictions: DataFrame = model.transform(test_data)

    val evaluator = new RegressionEvaluator()
      .setMetricName("rmse")
      .setLabelCol("rating")
      .setPredictionCol("prediction")
    val rmse = evaluator.evaluate(predictions)
    println(s"El error cuadrático medio de este estimador es: ${rmse}")

    val userRecs: DataFrame = model.recommendForAllUsers(5)
    val movieRecs: DataFrame = model.recommendForAllItems(5)

    movieRecs.as("recs").joinWith(movies.as("movies"), $"recs.movieId" === $"movies.movieId").select($"_1.*", $"_2.title").show(20)
    userRecs.as("mr").show(20)

  }
  def filePath(name: String) = {
    val resource = getClass.getClassLoader.getResource(s"ml-latest-small/${name}.csv")
    if (resource == null) sys.error("Please download the dataset as explained in the assignment instructions")
    new File(resource.toURI).getPath
  }
}
