import Ejercicio_02.Rational

object Ejercicio_D2_02 {
  def main(args: Array[String]): Unit = {

    trait MyOption[+T] {
      def isDefined: Boolean
      def map[R](f: T => R): MyOption[R]
      def flatMap[R](f: T => MyOption[R]): MyOption[R]
      def forall(p: T => Boolean): Boolean = !isDefined || p(get)
      def exists(p: T => Boolean): Boolean = isDefined && p(get)
      def get: T
    }

    case class MySome[+T](obj: T) extends MyOption[T] {
      override def isDefined: Boolean = true
      override def map[R](f: T => R): MyOption[R] = MySome(f(obj))
      override def flatMap[R](f: T => MyOption[R]): MyOption[R] = f(obj)
      override def get: T = obj
    }

    case object MyNone extends MyOption[Nothing] {
      override def isDefined: Boolean = false
      override def map[R](f: Nothing => R): MyOption[Nothing] = MyNone
      override def flatMap[R](f: Nothing => MyOption[R]): MyOption[R] = MyNone
      override def get: Nothing = throw new NoSuchElementException("None.get")
    }


    val m = Map("Suiza" -> "Berna", "España" -> "Madrid")

    val o = Option(1)
    o.flatMap(???)

    m.get("Portugal").map(_.toUpperCase).map(_.substring(0, 3)) match {
      case Some(city) => println(city)
      case None => () // Exception handling
    }

    implicit class MyOptionToOption[T](obj: MyOption[T]) {
      def toOption: Option[T] = obj match {
        case MySome(obj) => Some(obj)
        case MyNone => None
      }
    }

    val example = MySome(345)

    val exampleOption = example.toOption

    val example2Option = new MyOptionToOption(example).toOption

    List(new Rational(2, 3), new Rational(4, 5)).sorted((x: Rational, y: Rational) => implicitly[Ordering[Int]].compare(
      x.numer / x.denom,
      y.numer / y.denom
    ))



  }
}
