object Ejercicio_04 extends App {

  object Example {

    abstract class List[+T] {
      def isEmpty: Boolean

      def map[R](f: T => R): List[R]
    }

    object List {
      def apply[T](bindings: T*): List[T] = if (bindings.isEmpty) Nil else new ::(bindings.head, apply(bindings.tail : _* ))
    }

    List(1, 2, 3, 4)

    case object Nil extends List[Nothing] {
      override def isEmpty: Boolean = true

      override def map[R](f: Nothing => R): List[R] = Nil
    }

    case class ::[T](a: T, rest: List[T]) extends List[T] {
      override def isEmpty: Boolean = false

      def ++ (other: List[T]): List[T] = other match {
        case Nil => this
        case x :: xs =>  ::(x, this) ++ xs
      }

      override def map[R](f: T => R): List[R] = new ::(f(a), rest.map(f))

      // override def flatMap[R](f: T => List[R]): List[R] = f(a) ++ rest.flatMap(f))
    }

    // List(1, 2, 3, 4, 5).map((i: Int) => i * 2) == List(2, 4, 6, 8, 10)

    /*
    val x1 = new Cons[Int](1, Nil)
    val x2 = new Cons[String]("1", Nil)
    val x3 = scala.List(1, 2, 3, 4).map((i: Int) => i.toString)
    Nil.map((i: Int) => i * 2)
     */
    val x1 = new ::(1, Nil)
    val x2 = new ::("1", Nil)

    val stringifyInt = (i: Int) => i.toString

    val x3 = scala.List(1, 2, 3, 4).map(stringifyInt)

  }

  new Example.::(5, Example.Nil)


  val l1: List[Int] = List(1, 2, 3, 4, 5, 6)
  l1.map(_.toString)

  def f: (Int, Int) => Int = _ * _

  val s1 = Set(1, 2, 3)
  s1.contains(4)

  val m1 = Map(2 -> 5, 3 -> 5)
  m1.get(7)



  val m3: Map[String, String] = Map("Suiza" -> "Berna", "España" -> "Madrid")

  val m2: Map[String, Boolean] = Map("25-12" -> true).withDefaultValue(false)

  type Set = String => Boolean

  val m4: Set = m2


  println(l1)

  def even(x: Int) = x % 2 == 0
  val l2 = l1.filter(even)

  l2.flatMap((i: Int) => if (i % 2 == 0) List(i) else Nil)

  println(l2)




}
