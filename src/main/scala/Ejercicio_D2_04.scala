import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.storage.StorageLevel

object Ejercicio_D2_04 {

  def main(args: Array[String]): Unit = {
    val session: SparkSession = SparkSession.builder()
      .appName("Ejercicio_D2_04")
      .master("local[*]")
      .getOrCreate()

    // RDD = Resilient Distributed Dataset
    // val textFile: RDD[String] = session.sparkContext.textFile("src/main/resources/wikipedia/wikipedia.dat.gz")

    val arr = List(1, 2, 3, 4, 5)

    val x: RDD[Int] = session.sparkContext.parallelize(arr)

    val doubleX = x.map(_ * 2)
      .persist(StorageLevel.MEMORY_AND_DISK)

    val factorialX1: Int = x.reduce((i: Int, j: Int) => i * j)
    val factorialX: Int = doubleX.fold(1)(_ * _)
    val sumaExtranaX: Int = doubleX.fold(1)(_ + _)

    /*
    val z = doubleX.map(???).map(???).filter(???).cache()

    z.filter(???).collect()
    z.aggregate(???)(???)
*/

    val lastYearLogs: RDD[String] = session.sparkContext.parallelize(List(
      "2020-11-02T10:00:01 200 200 OK 192.168.0.1",
      "2020-11-02T10:00:01 200 200 OK database is replicating 192.168.0.1",
      "2020-11-02T10:00:01 200 500 Error of Database Access 192.168.0.1",
      "2020-11-02T10:00:01 200 200 OK DataBase replicated successfully 192.168.0.1",
      "2020-11-02T10:00:01 200 200 OK 192.168.0.1",
    ))
    val databaseLogs = lastYearLogs.map(_.toLowerCase).filter(_.contains("database")).persist()
    val databaseLogs_synthetic = lastYearLogs.flatMap { tmp0 =>
      val tmp1 = tmp0.toLowerCase
      if(tmp1.contains("database"))
        List(tmp1)
      else List()
    }.persist()

    println(databaseLogs.take(2).mkString("Array(", ", ", ")"))
    println("Total: " + databaseLogs.count())

    val a: (Int, Int) = (1, 2)

    val renfeLogs: RDD[String] = session.sparkContext.parallelize(List(
      "2020-11-02T10:00:01 1 MADRID BARCELONA AVE 50.00",
      "2020-11-02T10:00:01 2 MADRID MALAGA AVE 40.05",
      "2020-11-02T10:00:01 2 MALAGA MADRID AVE 40.05",
      "2020-11-02T10:00:01 1 BARCELONA MADRID LD 40.08",
    ))
    case class RenfeLog(timestamp: String, customerId: String, origin: String, destination: String, productId: String, price: String)
    val renfeLogsParsed: RDD[RenfeLog] = renfeLogs.map { s: String =>
      val t = s.split(" ")
      RenfeLog(t(0), t(1), t(2), t(3), t(4), t(5))
    }

    // 1. Contar los viajes que ha hecho cada cliente
    val logsByCustomer: RDD[(String, Int)] = renfeLogsParsed.map(log => (log.customerId, 1))
    val tripsByCustomer: RDD[(String, Int)] = logsByCustomer.reduceByKey((a: Int, b: Int) => a + b)

    val logsByOrigin: RDD[(String, RenfeLog)] = renfeLogsParsed.map(log => (log.customerId, log))
    logsByOrigin.mapValues(log => log.origin)
      .sortByKey()

    println(tripsByCustomer.collect().mkString("Array(", ", ", ")"))

    // val r: Array[String] = x.map(f => f.toUpperCase).collect()

  }

}
