import java.io.File
import java.util

import org.apache.spark.sql.{DataFrame, Row, SparkSession}

object Ejercicio_D3_03_Cached {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.builder()
      .appName("Ejercicio_D2_04")
      .master("local[*]")
      .getOrCreate()

    val pokemons: DataFrame = spark.read.option("header", true).option("inferSchema", true).csv(filePath).cache()

    pokemons.createOrReplaceTempView("pokemons")

    pokemons.printSchema()

    val result: util.List[Row] = spark.sql("SELECT Name, Attack FROM pokemons").takeAsList(10)

    // Qué pokemons tienen el mismo ataque que defensa
    val pokesSameAtkDef = spark.sql("SELECT * FROM pokemons WHERE Attack = Defense")
    pokesSameAtkDef.show()

    import org.apache.spark.sql.functions._
    import spark.implicits._

    val pokesSameAtkDef2 = pokemons.select($"*").where($"Attack" === $"Defense")


    // Cuántos pokemons tienen las mismas estadísticas de ataque?
    val pokesSameStatsAtkGroup = spark.sql("SELECT Attack, COUNT(*) AS NumPoke FROM pokemons GROUP BY Attack ORDER BY NumPoke DESC") //.show(20)
    pokesSameStatsAtkGroup.createOrReplaceTempView("pokemonsThatFit")
    spark.sql("SELECT * FROM pokemonsThatFit")
    pokemons.where($"Attack" === $"Defense").select(count($"*")).show() // TODO COMENTAR ESTO MAÑANA

    // Legendarios que tienen las mismas stats de atk y def que no legendarios
    val pokesLikeLegends: DataFrame = spark.sql(
      """SELECT *
        | FROM pokemons p1
        | JOIN pokemons p2
        | WHERE p1.Attack = p2.Attack
        | AND p1.Defense = p2.Defense
        | AND p1.Legendary
        | AND NOT p2.Legendary
        |""".stripMargin)

    pokesLikeLegends.show(50) // See that scan_csv number of output rows can change in plan

    System.in.read()
    spark.stop()

  }

  def filePath = {
    val resource = getClass.getClassLoader.getResource("pokemon.csv")
    if (resource == null) sys.error("Please download the dataset as explained in the assignment instructions")
    new File(resource.toURI).getPath
  }
}
