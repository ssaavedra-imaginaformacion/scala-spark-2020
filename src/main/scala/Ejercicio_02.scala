object Ejercicio_02 extends App {
  class Rational(x: Int, y: Int) {
    require(y > 0, "denominator must be positive")

    def this(x: Int) = this(x, 1)

    private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
    val g = gcd(x, y)

    def numer: Int = x / g
    def denom: Int = y / g

    def + (other: Rational): Rational =
      new Rational(
        numer * other.denom + other.numer * denom,
        denom * other.denom
      )

    // + - ! ~
    def unary_- : Rational = new Rational(-numer, denom)

    /**
     * This subtracts one rational from another
     * @param other
     * @return
     */
    def - (other: Rational): Rational = this + (-other)

    def <(other: Rational): Boolean = numer * other.denom < other.numer * denom

    def max(other: Rational): Rational = if (this < other) other else this

    // override def toString: String = x + " / " + y
    override def toString: String = s"${x} / ${y}"
  }

  val x = new Rational(1, 2)
  val y = new Rational(2, 3)
  val z = new Rational(6, 3)

  println(x + y)
  println(-z)
  println(x.-(y))
  println(x - y)

  5 * 3 + 4 * 8

  // (las letras)
  // ^
  // &
  // < >
  // = !
  // :
  // + -
  // * / %
  // (otros caracteres especiales)


}
