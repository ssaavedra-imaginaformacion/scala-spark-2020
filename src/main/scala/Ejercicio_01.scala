object Ejercicio_01 extends App {
    println("Hola, mundo")
}


// READ EVAL PRINT LOOP

//
//def pi = 3.14
//def radius = 10.0
//
//(3 * pi) * radius
//
//
//def square(x: => Int) = x * x
//
//def loop: Int = loop
//
//def first(one: Int, second: => Int) = one
//first(5, loop)
//
//def ifThenElse(boolean: Boolean, thenPart: Int, elsePart: String) =
//    if (boolean) thenPart else elsePart
//
//def b1 = true
//def b2 = false
//
//def and(x: Boolean, y: => Boolean): Boolean =
//    if (x) y else false
//
//def or(x: Boolean, y: => Boolean): Boolean =
//    if (x) true else y
//
//
