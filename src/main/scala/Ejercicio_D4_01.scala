import java.io.File

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

// #,Name,Type 1,Type 2,Total,HP,Attack,Defense,Sp. Atk,Sp. Def,Speed,Generation,Legendary
case class Pokemon(`#`: Int, Name: String, `Type 1`: String, `Type 2`: String, Total: Int, HP: Int, Attack: Int, Defense: Int,
                  `Sp. Atk`: Int, `Sp. Def`: Int, Speed: Int, Generation: Int, Legendary: Boolean)

case class StringInt(s: String, i: Int)

object Ejercicio_D4_01 {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.builder()
      .appName("Ejercicio_D2_04")
      .master("local[*]")
      .getOrCreate()

    import spark.implicits._
    import org.apache.spark.sql.functions._

    val pokemons: Dataset[Pokemon] = spark.read.option("header", true).option("inferSchema", true).csv(filePath).as[Pokemon].cache()
    pokemons.createOrReplaceTempView("pokemons")

    // Legendarios que tienen las mismas stats de atk y def que no legendarios
    spark.sql(
      """SELECT *
        | FROM pokemons p1
        | JOIN pokemons p2
        | WHERE p1.Attack = p2.Attack
        | AND p1.Defense = p2.Defense
        | AND p1.Legendary
        | AND NOT p2.Legendary
        |""".stripMargin).show(2)

    pokemons.as("p1").joinWith(pokemons.as("p2"), $"p1.Attack" === "p2.Attack" &&
      $"p1.Defense" === $"p2.Defense" &&
      $"p1.Legendary" && !$"p2.Legendary")

    val pokemonsAsLegendary = pokemons.as("p1").joinWith(pokemons.as("p2"), $"p1.Legendary" && !$"p2.Legendary")
      .filter($"p1.Attack" === "p2.Attack" && $"p1.Defense" === $"p2.Defense" && $"p1.Legendary" && !$"p2.Legendary")
      .filter(_ match { case (left: Pokemon, right: Pokemon) => left.Attack == right.Attack && left.Defense == right.Defense } )
      .select($"Name".as[String], ($"Attack" + $"Defense").as[Int])
      .withColumnRenamed("Name", "s")
      .withColumnRenamed("(Attack + Defense)", "i")
      .as[StringInt]

    val myToUppercase = udf((field: String) => field.toUpperCase)

    spark.udf.register("UPPERCASE", myToUppercase)

    // pokemonsAsLegendary.map(r)
    pokemonsAsLegendary.show(3)

    spark.sql("SELECT Name, (Attack + Defense) FROM pokemons").show(4)
    pokemons.select($"Name", ($"Attack" + $"Defense")).show(5)
    pokemons.withColumn("(Attack + Defense)", $"Attack" + $"Defense")
      .select($"Name", $"(Attack + Defense)").show(6)



    System.in.read()

  }

  def filePath = {
    val resource = getClass.getClassLoader.getResource("pokemon.csv")
    if (resource == null) sys.error("Please download the dataset as explained in the assignment instructions")
    new File(resource.toURI).getPath
  }
}