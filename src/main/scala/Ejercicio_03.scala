
object Ejercicio_03 {
  def set1(x: Int): Boolean = x == 1

  type Set = Int => Boolean

  def contains(s: Set, x: Int): Boolean = s(x)

  def union(a: Set, b: Set): Set = (x: Int) => a(x) || b(x)
  def intersection(a: Set, b: Set): Set = (x: Int) => a(x) && b(x)
  def diff(a: Set, b: Set): Set = (x: Int) => a(x) && !b(x)

  def filter(a: Set, predicate: Int => Boolean): Set =
    intersection(a, predicate)


  val z: Set = (x: Int) => x == 1



}
