import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

case class ContractRow(month_id: Int, contract_id: Int)

object Ejercicio_D4_02 {

  val months = 1 to 12
  val contratos = List((1, 2), (2, 2), (1, 3), (2, 4), (3, 2), (3, 3))


  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("ejemplo").master("local[*]").getOrCreate()
    import spark.implicits._


    val monthsDS = spark.sparkContext.parallelize(months).toDS()
    val contratosDS = spark.sparkContext.parallelize(contratos.map{ case (i, j) => ContractRow(i, j) }).toDS()
    monthsDS.createOrReplaceTempView("months")
    contratosDS.createOrReplaceTempView("contratos")

    val contractsMonthAndNext = contratosDS.joinWith(
      contratosDS.select(($"month_id" - 1).as[Int], $"contract_id".as("contract_id_2").as[Int]), $"contract_id" === $"contract_id_2"
    )

    val x: RDD[(Int, Vector[Boolean])] = contratosDS.sort($"month_id").rdd
      .keyBy(_.contract_id)
      .aggregateByKey(Vector((1 to 12).map(_ => false):_*))(
        (arr, contract) => arr.updated(contract.month_id, true),
        (a, b) => a.zip(b).map(t => t._1 || t._2))
      .filter { case (contract_id, vector) => vector.dropWhile(p => !p).dropWhile(p => p).nonEmpty }


        contratosDS.joinWith(contractsMonthAndNext, $"contract_id" === $"contract_id_2", "left_outer")

    (1 to 12).par.map(month => {
      spark.sql("SELECT * FORM contratos WHERE month_id = " +month+ " AND contract_id NOT IN (SELECT * FROM contratos WHERE month_id != " + month + ")")
        .collect()
    })

  }

}
