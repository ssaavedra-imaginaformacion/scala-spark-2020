import javax.management.AttributeNotFoundException

object Ejercicio_05 {
  trait Expr {
  }

  case class Number(n: Int) extends Expr

  case class Sum(e1: Expr, e2: Expr) extends Expr

  case class Product(e1: Expr, e2: Expr) extends Expr

  def eval(expr: Expr): Int = expr match {
    case Number(0) => 0
    case Number(n) => n
    case Sum(a, b) => eval(a) + eval(b)
    case Product(a, b) => eval(a) * eval(b)
  }

  def simplify(expr: Expr): Expr = expr match {
    case Sum(Product(a1, a2), Product(b1, b2)) => if (a1 == b1) Product(a1, Sum(a2, b2)) else expr
    case _ => expr
  }

}
