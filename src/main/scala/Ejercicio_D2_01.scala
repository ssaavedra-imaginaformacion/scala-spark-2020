import scala.annotation.tailrec
import scala.runtime.RichLong

object Ejercicio_D2_01 {

  // Contenido del curso:
  // https://cloud.ssaavedra.eu/s/TzmZJ2kXL7opGKB

  // Drop box ejercicios:
  // https://cloud.ssaavedra.eu/s/Aw44Bj5MZkndrDb

  // Tail recursion, bucles y variables
  def factorial1(n: Int): Int =
    if (n == 0) 1 else n * factorial1(n - 1)

  def factorial(n: Int): Int = {
    @tailrec
    def aux(n: Int, acc: Int): Int =
      if (n == 0) acc else aux(n - 1, acc = n * acc)
    aux(n, 1)
  }

  def factorialI(n: Int): Long = {
    var acc = 1
    (n to 1 by -1).foreach(i => acc * i)
    (n to 1 by -1).map(i => acc * i)
    (n to 1 by -1).flatMap(i => List(acc * i))
    (1 to n).reduce((i: Int, j: Int) => i * j)

    // val f: Long = (1 to n).reduceLeft((i: Long, j: Int) => i * j)

    (1 to n).reduceRight((i: Int, j: Int) => i * j)

    (1 to n).fold(1)((i: Int, j: Int) => i * j)
    (3 to n).fold(1 * 2 * 3)((i: Int, j: Int) => i * j)
    (4 to n).fold(1 * 2 * 3 * 4)((i: Int, j: Int) => i * j)
    // (n to 1 by -1).fold()

    (1 to n).foldLeft(1)((i: Int, j: Int) => i * j)
    (1 to n).product
    (1 to n).sum
    (1 to n).max
    (1 to n).min
    acc
  }

  def numbersToN(n: Int) = {
    // def foldLeft[B](z: B)(op: (B, A) => B): B
    val numbersToN: String = (1 to n).mkString(", ")
    println(s"Numbers to n: ${numbersToN}")
  }

  def flatten_x10(l: List[List[Int]]): List[Int] = {
    l.flatMap(i =>
      i.flatMap(j =>
        List(j, j * 10)))

    l.par
      .flatten // == .flatMap(i => i)
      .flatMap(j => List(j, j * 10))
      .toList
  }

  def main(args: Array[String]): Unit = {
    numbersToN(10)

    println(flatten_x10(List(
      List(1, 2, 3),
      List(4, 5, 6)
    ))) // List(1, 10, 2, 20, 3, 30, 4, 40, 5, 50, 6, 60)

    object X {
      import Ejercicio_02.{Rational => R}

      implicit object RationalsNumeric extends Numeric[Ejercicio_02.Rational] {
        override def plus(x: R, y: R): R = x + y

        override def minus(x: Ejercicio_02.Rational, y: Ejercicio_02.Rational): Ejercicio_02.Rational = x - y

        override def times(x: Ejercicio_02.Rational, y: Ejercicio_02.Rational): Ejercicio_02.Rational = x * y

        override def negate(x: Ejercicio_02.Rational): Ejercicio_02.Rational = -x

        override def fromInt(x: Int): Ejercicio_02.Rational = new Ejercicio_02.Rational(x)

        override def toInt(x: Ejercicio_02.Rational): Int = x.numer / x.denom

        override def toLong(x: Ejercicio_02.Rational): Long = x.numer.toLong / x.denom

        override def toFloat(x: Ejercicio_02.Rational): Float = x.numer.toFloat / x.denom

        override def toDouble(x: Ejercicio_02.Rational): Double = x.numer.toDouble / x.denom

        override def compare(x: Ejercicio_02.Rational, y: Ejercicio_02.Rational): Int = if (x < y) -1 else 1
      }

    }
    import X.RationalsNumeric // Implicit import

    println(
      List(new Ejercicio_02.Rational(2, 3), new Ejercicio_02.Rational(2, 3)).product
    )
  }

}
