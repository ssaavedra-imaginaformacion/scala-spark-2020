object Ejercicio_D2_03 {

  trait BinaryTree {
    def isEmpty: Boolean
    def height: Int
  }

  case class NonEmptyBinaryTree(value: Int, leftNode: BinaryTree, rightNode: BinaryTree) extends BinaryTree {
    override def isEmpty: Boolean = false
    override def height: Int = 1 + math.max(leftNode.height, rightNode.height)
    override def toString: String = s"➡️[${value}], ${leftNode}, ${rightNode}⬅️"
  }

  case object EmptyBinaryTree extends BinaryTree {
    override def isEmpty: Boolean = true
    override def height: Int = 0
    override def toString: String = "\uD83C\uDF31"
  }

  def isBalanced(tree: BinaryTree): Boolean = tree match {
    case EmptyBinaryTree => true
    case NonEmptyBinaryTree(_, left, right) =>
      val diff = math.abs(left.height - right.height)
      if (diff > 1)
        false
      else isBalanced(left) && isBalanced(right)
  }

  def main(args: Array[String]): Unit = {
    assert(isBalanced(NonEmptyBinaryTree(5, EmptyBinaryTree, EmptyBinaryTree)))
    assert(isBalanced(NonEmptyBinaryTree(5, NonEmptyBinaryTree(5, EmptyBinaryTree, EmptyBinaryTree), NonEmptyBinaryTree(5, EmptyBinaryTree, EmptyBinaryTree))))
    assert(!isBalanced(
      NonEmptyBinaryTree(5,
        EmptyBinaryTree,
        NonEmptyBinaryTree(5,
          NonEmptyBinaryTree(5,
            NonEmptyBinaryTree(5, EmptyBinaryTree, EmptyBinaryTree),
            EmptyBinaryTree),
          EmptyBinaryTree))))

    println(NonEmptyBinaryTree(5, EmptyBinaryTree, EmptyBinaryTree))
    println(NonEmptyBinaryTree(5, NonEmptyBinaryTree(5, EmptyBinaryTree, EmptyBinaryTree), NonEmptyBinaryTree(5, EmptyBinaryTree, EmptyBinaryTree)))
    println(NonEmptyBinaryTree(5, EmptyBinaryTree, NonEmptyBinaryTree(5, NonEmptyBinaryTree(5, NonEmptyBinaryTree(5, EmptyBinaryTree, EmptyBinaryTree), EmptyBinaryTree), EmptyBinaryTree)))
  }


}
